# À propo de « Info pour les nuls »

« Info pour les nuls » est un cycle de conférences sur l'informatique à destination des collègues mathématiciens de l'Université de Lille.

# À propo de ce blog

Ce blog est un carnet de notes des exposés de « Info pour les nuls ».